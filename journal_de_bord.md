##Journal de bord (Français)

##Première étape:L'addition

###Operators

####Addition

On suppose que seulement l'addition nous est donnée et on fait une addition
par récurrence.

####Généralisation *

On crée une table de hachage qui associe le chaîne de caractère d'un operator
à la fonction à effectuer sur deux nombres (std::plus<int>())

Ainsi on peu créer une fonction qui prend en entrée le nom de l'operator
et qui applique l'opération associée sur les deux opérandes.

On a ainsi évité d'écrire beaucoup de lignes de code, et l'ajout d'opérator
est devenu très simple.

####Macros
On distingue deux types de macros: quote et nil, et les autres.
Pour quote et nil, l'objectif est de remplacer syntaxiquement'() par
(quote ()), de même pour nil, le tout dans le parseur; ce n'est pas possible
pour les autres.
Pour les autres, on se concentrera dessus lors d'une deuxième étape de création
de macro.

####Macro nil
Un problème s'est posé du fait que nil avait été présenté comme un symbole : 
le token_nil ou la liste vide entrainaient la création d'une cellule "nil()" 
qui était de type Cell_Symbol. Nous avons décidé de régler ce problème en
créant une classe fille de Cell nommée Cell_Empty. C'est plus propre, et ça
permet de mieux expanser la macro nil, NIL ou Nil.

####Environnement *

On crée un objet qui ressemble à une table d'association.
Nous décidons d'utiliser des chaînes de caractères au lieu d'objets symboles 
comme clefs, car il serait possible de passer autre chose
que des objets symbole ce qui est dangeureux (et car l'evaluation a pour
devoir de faire le passage de Lisp à C++ donc autant centraliser ce travail
dans eval et ne pas mettre la transformation de symbol à string  dans
l'environnement).

####Environnement 2
Retrouvé éxactement l'idée.

Jean a suggéré de changer la structure de donnée de l'environnement que Dylan 
avait implémenté. En effet, la structure de donnée choisie était la liste de 
liens, ce qui n'est pas exactement la structure de pile d'environnements vue 
en cours. Dylan le fera plus tard. D'ailleurs, les spécifications de Jean sur 
la classe environnement (3 méthodes : get_value, extend et add_link)
n'imposaient pas ce changement.

##Environment 3
Je n'avais pas compris le sens de Environment_base. Il est indispensable pour
pouvoir avoir un environnement dans lequel on puisse choisir
Environment_Empty, ce qui n'est pas possible dans la classe Environment.

On utilise un Handle pour gérer les environnement ainsi on peut facilement
ajouter Link

####Documentation
Ca marche mieux avec param qu'avec var.

####Mots clefs *
Les mots clef tel que let, if, t, peuvent être implémentés directement en
tant que token et cell (Token_if dans le lexer, Cell_if dans le code et
parser), cela permet des erreurs de sémantiques lorsque l'utilisateur essaie
d'assigner des valeurs sur ces objets. Cela permet de rendre l'interpréteur
robuste mais peu modulaire. En choisissant de regarder lors de l'évaluation
si un symbol est un if, un t ou un let on permet d'être plus modulaire
(seulement eval doit être changé, avant on devait changer le lexer, le parser,
cell et eval) et si l'utilisateur ajoute dans l'environnement des liens
sur ces symboles il ne seront pas considérer, et donc les programmes vont
garder leurs sens. Cela reste donc relativement robuste.

####Compte-rendu de la première démonstration
Nathanaël nous conseille git fetch -a
Luc nous a dit de régler: quote, les fuites mémoires, les fonctions,
l'environnement. Attention à ajouter std c++14.

##Apply *
L'implémentation d'apply se fait en copiant le *handle* de l'environnement
courant et en y ajoutant l'environnement qui contient les liens entre
les paramètre de la fonction et les arguments de la fonctions. 

Ainsi on ne change pas l'environnement avant le lancement de la fonction

Si la fonction est un symbole, nous récupérons sa valeur dans l'environnement
et essayons d'appliquer cette valeur sur les arguments avec l'environnement
courant de nouveau

Si un symbole est lié à une opération la fonction apply ne voudra pas
appliqué le symbole, car la fonction apply ne veut pas appliqué les opération
(géré par eval) devrait-on gérer les opération avec apply.

####Compte-rendu de la première démonstration
Nathanaël nous conseille git fetch -a
Luc nous a dit de régler: quote, les fuites mémoires, les fonctions,
l'environnement. Attention à ajouter std c++14.
"Vous allez probablement faire un méta-évaluateur."
"Je t'emmerde, je fais ce que je veux."


##Garbage Collector *

The garbage collector will memorise the allocated objects and destroy
the allocated object that are not in a given environment when asked.

Lorsqu'un lien cache un notre lien, il est peut être important de le suprimer
pour gagner de la mémoire.

Le garbage collector va devoir parcourire tout l'environnement pour chaques
objet enregistré et ainsi savoir si il doit etre retirer. L'environnement
n'est plus géréer comme cela:

Pour remédier à cela on ajoute un état à l'intérieur d'une cellule qui
indique si elle est utilisé ou non (à l'intérieur d'un environnement),
Ainsi avant de clean le garbage collector met à jour les états des objets.

Peut être pouvons nous faire en sorte que ce soit un invariant. En activant
l'état lorsque il est ajouté à l'environnement et désactivé lorque 
l'environnement est détruit, il sera implémenté plus tard. C'est ce qui a
été implémenté

Dylan a pensé à ajouter un constructeur dans Cell
qui change l'état de l'objet, cela
ne parait pas approprier à Jean car le fait qu'un objet est busy où non
est du au fait qu'il est ajouté à un environnement, ce qui se passe après
la construction.

Jean pense qu'il faudrait mettre Environnement en amis à Cell pour ainsi
il soit le seul à pouvoir changer l'état de busy de Cell du au fait que l'état
est lié à l'environnement et l'environnement seul. Mais Dylan pense qu'il ne
faut pas le faire car Luc Bougé ne veut pas que l'on accède aux cellule
dirrectement mais sous la forme d'objet.

Après une discution avec Nathanaël la mémoire doit être séparé du garbage
collector, car le garbage collector ne s'occupe que de savoir quoi enlevé
mais pas de l'enlevé ni de la créer. La mémoire pourrait être géré par une
factory qui stockerait les cellules et aurait une interface vers ces cellules
ils s'occuperait aussi de créer les cellules.

##Exceptions *

Nous avons déscidé de lancer des exceptions au plus haut niveau possible:
dès qu'on peut savoir si il est nécessaire d'envoyer une exception, on ne
l'envoie pas dans des sous fonctions. Cela permet de centraliser au maximum
la façon dont on gère les exceptions.

We created an exceptions class for each different errors, this way, we are
able to process the error in a case by case if needed (e.g. EndOfFile must
be handled differently from the other exception since the interpret must
be ended)

A special error is created for the error linked to the primitive functions
because we know that these function name when we see the error.
