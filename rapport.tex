\documentclass[a4paper,11pt]{article}%

\usepackage{fullpage}%
\usepackage[T1]{fontenc}%
\usepackage[utf8]{inputenc}%
\usepackage[main=francais,english]{babel}% 

\usepackage{minted}
\usemintedstyle{bw}

\usepackage{graphicx}%
\usepackage{url}%
\usepackage{abstract}%

%\usepackage{newpxtext, newpxmath}

\parskip=0.5\baselineskip

\sloppy

\newcommand{\languageen}[1]{\emph{#1}}

\begin{document}

\title{Un interprète Lisp en C++}

\author{Dylan Bellier \and Jean Jouve \and Axel Palaude}

\maketitle

\begin{abstract}

  \begin{description}
  \item[Mots-clés:]Interpéteur; Lisp; C++.     
  \item[Classification ACM:]Theory of computation; Semantics and reasoning;    
  Program reasoning; Program specification.

  \end{description}

\end{abstract}

\section*{Introduction}

Un interprète est un outil chargé d'analyser du code écrit dans un 
cerain langage afin de le traduire et de l'exécuter dans un autre. Nous sommes 
chargés d'implémenter un interprète \languageen{Lisp} en \languageen{C++}, 
c'est-à-dire un interprète codé en \languageen{C++} exécutant du code 
\languageen{Lisp}. Pour réaliser l'implémentation, nous partons d'un analyseur lexical et 
d'un analyseur syntaxique et nous permettons l'interprétation des objets fournis afin de 
pouvoir exécuter du code. Pour cela, nous créons des environnements, ainsi
qu'une fonction d'évaluation chargée d'évaluer les objets \languageen{Lisp} 
envoyés par le analyseur syntaxique, et une autre chargée d'appliquer les fonctions créées 
dans l'interprète. Enfin, nous gérons les exceptions classiques d'un 
interprète pour lui éviter de s'arrêter à la moindre erreur en entrée.

\section{Outils à disposition}

Pour créer notre interprète \languageen{Lisp},
certains outils fondamentaux ont été mis à notre disposition. 
Tout d'abord, nous disposons d'un analyseur lexical et d'un analyseur syntaxique. 
Le premier permet de reconnaître des structures lexicales dans une suite de 
caractères et de les convertir en \emph{tokens} qui seront envoyés à l'analyseur 
syntaxique. Celui-ci est en charge de convertir les \emph{tokens} en objets 
utilisables par le reste de l'interprète.

Plusieurs classes permettent de représenter les objets lisp en \languageen{C++}: 
\mintinline{c++}{Cell} et les classes qui en héritent. Elles sont opaques pour 
les autres fichiers. On utilise \mintinline{c++}{Object}, qui est un alias pour les 
pointeurs vers des \mintinline{c++}{Cell} pour le reste du programme. Des
fonctions essentielles sur les objets sont tout de même données, comme
\mintinline{c++}{get_sort} qui permet de savoir le type de l'objet.


\section{Contribution}

  \subsection{Fonctionnement général de l'évaluation des expressions}
    Le langage \languageen{Lisp} nécessite un certain nombre de mots-clés
    pour fonctionner, comme les choix en fonctions de conditions 
    (\mintinline{scheme}{if}) ou les définitions de fonctions
    (\mintinline{scheme}{lambda}).
    Nous avons deux possibilités pour les introduire dans notre interprète.
    On peut introduire un token pour chaque mot-clé, ce qui empêche 
    l'utilisateur d'assigner des valeurs aux mots-clés. Ainsi, les mots-clés 
    gardent leur sens, cependant cela demande de modifier beaucoup de 
    fichiers (dans l'analyseur lexical et l'analyseur syntaxique).
    La deuxième solution est de laisser l'analyseur lexical et l'analyseur
    syntaxique intacts, et de tester le nom des symboles lors de l'évaluation
    pour savoir si ce sont des mots-clés. De cette façon, seul le fichier
    contenant l'évaluation demande d'être modifier. Cependant, l'utilisateur 
    peut confondre ses variables avec des mots-clés.
    Le principal objectif étant d'éviter tant que possible les erreurs dans
    l'interprète et non dans le code de l'utilisateur, nous choisissons 
    la deuxième possibilité.
		
		
    Nous introduisons comme demandé des opérateurs arithmétiques et des
    opérateurs de comparaison. Les opérateurs sont gérés de la même façon que
    les mots-clés: nous testons les symboles pour savoir si ils représentent
    un opérateur. Nous associons à chaque symbole d'opérateur
    une action à faire sur les arguments. Comme les actions prennent en
    entrée deux entiers, elles font toutes les même conversions: nous 
    introduisons donc une table de hachage qui associe le symbole de 
    l'opérateur à l'action, représentée par un objet fonctionnel. Ainsi, 
    l'ajout d'opérateur est centralisé dans une table de hachage, et la 
    conversion est centralisée dans une fonction.

  \subsection{Environnement}
    Un environnement est un ensemble de liaisons entre symboles et valeurs.
    Il faut pouvoir accéder à une valeur à partir d'un unique symbole, 
    c'est pourquoi les liaisons sont ordonnées selon leur ordre inverse 
    d'entrée dans l'environnement. Ainsi, si deux liaisons ont le même symbole, 
    seule la dernière ajoutée comptera. Ce phénomène s'appelle le masquage. 
    
    Pour implémenter les environnements, nous avons tout d'abord créé une 
    classe \mintinline{c++}{Link} qui représente une liaison entre un symbole et sa 
    valeur dans l'environnement. Un environnement est alors simplement 
    une liste chainée de liaisons. De cette façon, la propriété de masquage 
    est vérifiée. Pour des raisons pratiques, nous n'utilisons pas directement 
    les environnements mais leur préférons une \emph{poignée} (c'est un 
    pointeur vers un environnement) qui est plus pratique pour ce genre de 
    structure. Ainsi, nous pouvons facilement étendre temporairement 
    l'environnement puis retourner ultérieurement à l'environnement de départ.

  \subsection{Application des fonctions}
    Pour que l'interprète soit complet, les fonctions sont nécessaires
    Les fonctions en \languageen{Lisp} sont de la forme
    \mintinline{scheme}{(lambda (<parameters>) <corps>)} et l'application est
    de la forme \mintinline{scheme}{(<function> <arguments>)}.
    
    Nous voulons qu'une fonction
    puisse être stockée dans un symbole pour faciliter l'écriture de programme
    pour l'utilisateur. Ainsi, lorsque l'on veut appliquer une fonction, nous
    vérifions si la fonction en question est un symbole. Si c'est le cas
    il nous suffit d'évaluer le symbole et d'appliquer la fonction liée au
    symbole. Mais il faut qu'il soit possible de lier un symbole à
    un autre symbole, lui-même lié à une fonction, si ce n'est plus. Nous
    évaluons donc les symboles de façon récursive jusqu'à retrouver une
    fonction.
    
    Pour l'application de la fonction, nous voulons évaluer son corps
    en liant les symboles apparaissant dans les paramètres aux arguments
    associés. Nous copions donc la poignée vers l'environnement courant pour 
    récupérer une nouvelle poignée,
    puis nous étendons l'environnement de la nouvelle poignée pour qu'il
    contienne une liaison pour chaque paramètre et argument. Le corps de la
    fonction est ensuite exécuté dans l'environnement créé. L'environnement
    dans lequel la fonction est appelé ne change pas car seulement la
    poignée copiée est modifiée. De plus
    copier la poignée ne copie pas l'environnement, ce qui permet de
    garder de bonnes performances même pour des programmes qui utilisent
    intensivement les fonctions.
 
  \subsection{Gestion des exceptions}
Les exceptions permettent d'appréhender des erreurs de sémantique dans le code 
Lisp fourni. Elles sont en général affichées via un message d'erreur sur
l'interprète. Nous décidons de lancer les exceptions au plus haut niveau 
possible, pour ne pas les envoyer dans des sous-fonctions. Ce mode de 
fonctionnement nous permettant de centraliser les exceptions, nous les faisons 
dériver de la même exception runtime\_error présente dans C++, ce qui est un 
atout pour la modularité du code.

\section{Validation}

\subsection{Fonctionnement général de l'évaluation des expressions}

\paragraph{Surcharge des opérateurs} Dans l'interprète,
il est impossible d'utiliser un nom réservé à un opérateur comme un nom de 
fonction. En effet, la liaison entre le nom de l'opérateur et l'élément choisi
est bien effectuée, mais le rôle initial de l'opérateur est prioritaire lors
de l'exécution. Il n'y a donc pas de surcharge des opérateurs (voir 
\ref{surcharge}).

\subsection{Environnement}

\paragraph{Masquage} Lorsqu'on crée une liaison, elle est ajoutée à 
l'environnement. Si on crée une liaison ayant le même nom qu'une autre, les 
deux liaisons sont bien conservées dans l'environnement, et c'est bien la plus 
récente qui est considérée (voir \ref{masquage}). De plus, le changement d'un 
élément lié dans une fonction ou dans un let est bel et bien temporaire 
(voir \ref{masquage temporaire}).

\subsection{Apply}

    L'application d'une fonction n'est pas modifié ou inhibé par le renommage 
    de fonction (voir \ref{renommage de fonction}). De plus la programmation 
    de fonctions récursives se passe correctement. Par exemple la fonction 
    fibonacci, qui est doublement récursive, renvoie le bon résultat (voir 
    \ref{fonction récursive}.


\subsection{Gestion des exceptions}
La dérivation de \mint{c++}|runtime\_error| est utile pour la gestion d'erreurs 
spécifiques demandant un traitement particulier, comme par exemple 
\mint{scheme}|EndOfFile| qui doit arrêter l'interprète. Une autre exception 
particulière est liée aux fonctions primitives: l'exception permet de connaître 
le nom de la fonction lors de l'affichage de l'erreur (voir \ref{exceptions}).

\section{Conclusion}

Nous avons finalement implémenté l'interprète Lisp, mais nous n'avons
pas réalisé de récupérateur de mémoire par manque de temps. Par la suite, il 
serait donc intéressant de l'implémenter. Nous pourrions également modifier
l'interprète pour qu'il utilise des liaisons statiques. Enfin, l'idée d'un 
méta-interprète comme proposé dans le sujet est assez attrayante.

\appendix

\section*{Auto-évaluation}

\begin{description}
\item[Forces.] Nous avons réussi à nous répartir les tâches correctement, 
en appliquant la méthode \textit{AGILE} avec une bonne connaissance des 
compétences de chacun.
\item[Faiblesses.] Malgré cette bonne organisation, nous n'avons pas été assez
rapide et nous n'avons donc pas pu implémenter un récupérateur de mémoire.
\item[Opportunités.] L'absence du récupérateur de mémoire ne nous a pas empêché
de comprendre beaucoup mieux son principe de fonctionnement; Nous avons 
également compris comment fonctionnent les analyseur lexical et les analyseur syntaxique. 
\item[Menaces.]Les faiblesses individuelles de chacun (écriture de code C++, 
rédaction de rapports) sont à surveiller pour de futurs travaux en équipe.
\end{description}

\appendix
\section*{Annexe}

\begin{figure}
\label{surcharge}
    \begin{minted}{scheme}
      Lisp? (setq + *)
      ()
      (+ 1 2)
      3
      Lisp? (setq if +)
      ()
      Lisp? (if t 1 2)
      1
    \end{minted}
\caption{La surcharge des opérateurs est impossible}
\end{figure}

\begin{figure}
\label{masquage}
    \begin{minted}{scheme}
      Lisp? (setq a 1)
      ()
      Lisp? a
      1
      Lisp? (setq a 2)
      ()
      Lisp? a
      2
    \end{minted}
\caption{Le masquage est assuré}
\end{figure}

\begin{figure}
\label{masquage temporaire}
    \begin{minted}{scheme}
      Lisp? (setq a 1)
      ()
      Lisp? (let ((a 2)) a)
      2
      Lisp? a
      1
    \end{minted}
\caption{L'environnement global n'est pas affecté par l'utilisation 
d'environnement temporaire}
\end{figure}

\begin{figure}
\label{fonction récursive}
    \begin{minted}{scheme}
      Lisp? (defun fib (n)
        (if (< n 2)
          1
          (+ (fib (- n 1)) (fib (- n 2)))))
      ()
      Lisp? (fib 4)
      5
    \end{minted}
\caption{L'utilisation de fonction récursive est permise par notre interprète}
\end{figure}

\begin{figure}
\label{renommage de fonction}
    \begin{minted}{scheme}
      Lisp? (setq f g)
      ()
      Lisp? (defun g (a b) (+ a b))
      ()
      Lisp? (f 1 2)
      3
    \end{minted}
\caption{Il est possible de renommer une fonction sans perturber son 
application}
\end{figure}

\begin{figure}
\label{exceptions}
    \begin{minted}{scheme}
      Lisp? (defun f (a) a)
      ()
      Lisp? (f)
      Missing arguments
      Lisp? (progn)
      progn must have at least 1 arguments
      Lisp? ^D
      End of file
    \end{minted}
\caption{Les erreurs courrantes ne font pas quitter l'interprète mais End 
of file le fait}
\end{figure}

\end{document}

