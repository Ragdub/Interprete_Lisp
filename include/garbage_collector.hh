/**\file garbage_collector.hh
 * \brief File with definitions of th garbage collector.
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */

#pragma once

#include "object.hh"
#include "environment.hh"

#include <vector>

class GarbageCollector {
public:
    /** \return The vector representing memory. */
    static GarbageCollector& get_singleton();
private:
    static GarbageCollector singleton;
    std::vector <Object> object_collection;
    GarbageCollector() = default;
public:
    /** Add the object l to the current memory.
     * \param l an object that was just created. */
    void add_object(const Object& l);
    
    /** Destroy all cell that are unused.*/
    void clean();
};
