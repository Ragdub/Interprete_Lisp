#pragma once

#include "object.hh"

std::size_t get_list_length(const Object& object);
bool is_symbol_list(const Object& object);

Object list_car (const Object& obj);

Object list_cadr (const Object& obj); 
