#pragma once

#include <stdexcept>
#include <string>

class EndOfFileError : public std::runtime_error {
public:
    EndOfFileError();
};

class LambdaMissingError : public std::runtime_error {
public:
    LambdaMissingError();
};

class UnboundVariable : public std::runtime_error {
public:
    UnboundVariable(const std::string& symbol);
};

class VariablesMissingError : public std::runtime_error {
public:
    VariablesMissingError();
};

class ApplyNonListError : public std::runtime_error {
public:
    ApplyNonListError();
};

class TooManyArgumentError : public std::runtime_error {
public:
    TooManyArgumentError();
};

class MissingArgumentError : public std::runtime_error {
public:
    MissingArgumentError();
};

class WrongArgumentError : public std::runtime_error {
public:
    WrongArgumentError();
};

class PrimitiveWrongNumberOfArgumentError: public std::runtime_error {
public:
    PrimitiveWrongNumberOfArgumentError(
            const std::string& function_name,
            int number_needed_arguments);
};

class PrimitiveWrongArgumentsError: public std::runtime_error {
public:
    PrimitiveWrongArgumentsError(
            const std::string& function_name,
            const std::string& needed_type);
};

