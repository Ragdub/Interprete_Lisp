/**\file cell.hh
 * \brief File with definitions of Cell types
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
 
#pragma once

#include <string>

/*****************************/

/**The Cell class is the main class. 
 * Each Cell has a type: it can be a number, a string, a symbol or a pair.
 * Each type of cell is detailed in its specific daughter class.
 */

class Cell {
private:
    bool busy = false;
public:
    /**5 types of cells are recognized by the interpretor.
    */ 
    enum class sort {NUMBER, STRING, SYMBOL, PAIR, EMPTY};
    /** This function is a virtual function which is used to sort a cell, 
    * depending on its type. 
    */
    virtual sort get_sort() const = 0;
    virtual ~Cell() = default;
    
    /**A busy cell is a cell wich is can be accessed at the top level.
     * \return True if the cell is accessible.
     */
    bool is_busy() const;
    
    /**Change the attribute "busy" of the cell to \state.
     * \param A boolean \state.
     */
    void set_busy(const bool& state);
};

/*****************************/

/**The class Cell_Number is used for signed integers.
 */
 
class Cell_Number: public Cell {
private:
    int contents; /*! The content of the cell. It is an integer.*/
public:
    /** By using this function, you can obtain the type of the cell.
    * It is a NUMBER.
    */
    sort get_sort() const override;
  
    /**By using this function, you can choose the content of your cell.
    * \param content The content of the cell. It is an integer.
    */
    explicit Cell_Number(int _contents);
  
    /**By using this function, you can have the content of the cell.
    * \return The content of the cell.
    */
    int get_contents() const;
};

/*****************************/

/**The class Cell_String is used for strings.
 * \warning This cell requires a double quote at the beginning and the end 
 * of the string.
 */

class Cell_String: public Cell {
private:
    std::string contents; /*! The content of the cell. It is a string. */
public:
    /** By using this function, you can obtain the type of the cell.
    * It is a STRING.
    */
    sort get_sort() const override;
  
    /**By using this function, you can choose the content of your cell.
    * \param content The content of the cell. It is a string.
    */
    explicit Cell_String(const std::string& s);
  
    /**By using this function, you can have the content of the cell.
    * \return The content of the cell.
    */
    std::string get_contents() const;
};

/*****************************/

class Cell_Symbol: public Cell {
private:
    std::string contents;
public:
    /** By using this function, you can obtain the type of the cell.
    * It is a SYMBOL.
    */
    sort get_sort() const override;
  
    /**By using this function, you can choose the content of your cell.
    * \param content The content of the cell.
    */
    explicit Cell_Symbol(const std::string& s);
  
    /**By using this function, you can have the content of the cell.
    * \return The content of the cell.
    */
    std::string get_contents() const;
};

/*****************************/

class Cell_Pair: public Cell {
private:
    Cell *car; /*!The first parameter of the cell (car).*/
    Cell *cdr; /*!The secon parameter of the cell (cdr)*/
public:

    /** By using this function, you can obtain the type of the cell.
    * It is a PAIR.
    */
    sort get_sort() const override;
  
    /**By using this function, you can choose the content of your cell.
    * \param content The content of the cell. It is a pair.
    */
    explicit Cell_Pair(Cell *_car, Cell *_cdr);
  
    /**By using this function, you can have the first parameter of the cell.
    * \return The content of the first parameter of the cell (car).
    */
    Cell *get_car() const;
  
  /**By using this function, you can have the second parameter of the cell.
   * \return The content of the second parameter of the cell (cdr).
   */
  Cell *get_cdr() const;
};


/*****************************/

class Cell_Empty: public Cell {
public:
    /** By using this function, you can obtain the type of the cell.
    * It is an EMPTY cell.
    */
    sort get_sort() const override;
};

