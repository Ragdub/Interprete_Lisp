/**\file environment.hh
 * \brief File with definitions of Environment functions.
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */

#pragma once

#include <string>
#include <list>
#include <cassert>
#include <memory>

#include "object.hh"
#include "cell.hh"


class Link {
private:
    std::string symbol; /*!The symbol linked to a value.*/
    Object value; /*!The value attached to the symbol.*/
public:
	Link (const std::string& _symbol, const Object& _value);
    /** \return The symbol of the link.*/
    std::string get_symbol() const;
    /** \return The value attached to the symbol.*/
    Object get_value() const;
};

/*****************************/

/**
 * All environment must be created within a shared_ptr otherwise a
 * bad_weak_ptr will be thrown for all operations that extends the
 * environment.
 */

class Environment_Base
    : public std::enable_shared_from_this<Environment_Base> {
public:
	/**Boolean function.
	 * \return True if the Environment is empty.*/
    virtual bool is_empty() const = 0;

    /**Add all of the informations of a chosen environment to the Environment.
     * */
    virtual std::shared_ptr<Environment_Base> extend(
            const std::shared_ptr<Environment_Base>& environment) = 0;

    /**This function returns the most recent \value added in the environment 
     * for the symbol \symbol
     * \return The value linked to the chosen symbol.
     * */
    virtual Object get_value (const std::string& _symbol) const = 0;
    
    /**This function adds a link in the environment but does not remove 
     * eventual previous links for the same symbol.
     * \return a pointer of the resulting Environment.
     * \param _symbol The symbol to link
     * \param value The value linked to the symbol.
     * */
    std::shared_ptr<Environment_Base> add_link(
            const std::string& _symbol,
            const Object& value);
};

/*****************************/

class Environment_Empty: public Environment_Base {
public:
	/**Boolean function.
	 * \return True if the Environment is empty.*/
    bool is_empty() const override;


    /**Add all of the informations of a chosen environment to the Environment.
     * */
    std::shared_ptr<Environment_Base> extend(
            const std::shared_ptr<Environment_Base>& environment) override;


    /**\throw An error because if you search a value in an empty environment, 
     * you won't fine anything. */
    Object get_value (const std::string& _symbol) const override;
};

/*****************************/

class Environment: public Environment_Base {
private:
    Link car;
    std::shared_ptr<Environment_Base> cdr;
public:
    Environment(
            const Link& _link,
            const std::shared_ptr<Environment_Base>& _links);
    
    ~Environment();
    
    /**Boolean function.
	 * \return True if the Environment is empty.*/
	bool is_empty() const override;
	
    /**Add all of the informations of a chosen environment to the Environment.
     * */
    std::shared_ptr<Environment_Base> extend(
            const std::shared_ptr<Environment_Base>& environment) override;

    /**This function returns the most recent \value added in the environment 
     * for the symbol \symbol
     * \return The value linked to the chosen symbol.
     */
    Object get_value (const std::string& _symbol) const override;
};
/*****************************/

class Handle {
private:
    std::shared_ptr<Environment_Base> environment;
public:
    Handle();
    Handle(const Handle& other) = default;

    /**Extend the handled environment with the parameter(s) of a function and 
     * evaluated on the argument(s).
     * \param parameter is the object containing all the symbols of links 
     * to add.
     * \param argument is the same-size as Parameter. It contains the values 
     * with wich are linked the symbols.
     */
    void extends_with_parameter_arguments(
            const Object& parameter,
            const Object& arguments);

    /**Add a link in the handled environment.
     * \param _symbol is the symbol of the link.
     * \param value is the value of the link
     */
    void add_link(const std::string& _symbol, const Object& value);
    
    /**
     * \return The value of the symbol in the handled environment.
     */
    Object get_value(const std::string& _symbol) const;
};
