/**\file evaluation.hh
 * \brief File with definition of evaluation functions.
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
 
 #pragma once

#include "object.hh"
#include "environment.hh"

/**Evaluate an object according to Lisp semantic
 * \param object The object to evaluate.
 * \return The evaluated object.
 */
Object eval(const Object& object, Handle& current_environment);
Object map_eval(const Object& list, Handle& current_environment);

Object get_expansion_of_let (const Object& obj);

Object apply(
        const Object& function,
        const Object& arguments,
        const Handle& current_environment);
