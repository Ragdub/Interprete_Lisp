/**\file read.hh
 * \brief Definition of the methods of reading functions.
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
 
#pragma once

#include "object.hh"


Object read_object();

void read_object_restart(FILE* fh);
