/**\file subroutine.hh
 * \brief File with definitions of subroutine functions.
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
 
#pragma once

#include "object.hh"

/**Return if a symbol is a subroutine.
 * \param symbol A string representing the symbol
 * */
bool is_subroutine(const std::string& symbol);

/**Appliy a subroutine.
 * \param symbol The symbol indicates which subroutine will be used.
 * \param lhs The left-hand side of the operation. It is an object.
 * \param rhd The right-hand side of the operation. It is an object.
 * \return An object, which is the result of the chosen subroutine.
 * */
Object apply_subroutine(
        const std::string& symbol,
        const Object& lhs,
        const Object& rhs);

/**Return if a symbol is an operator.
 * \param symbol A string representing the symbol
 * */
bool is_operator(const std::string& symbol);

/**Apply an operation.
 * \param symbol The symbol indicates which operation will be used.
 * \param lhs The left-hand side of the operation. It is an object.
 * \param rhd The right-hand side of the operation. It is an object.
 * \return An object, which is the result of the chosen operation.
 * */
Object apply_operator(
        const std::string& symbol,
        const Object& lhs,
        const Object& rhs);

/**Return if a symbol is a subroutine.
 * \param symbol A string representing the symbol
 * */
bool is_comparison(const std::string& symbol);

/** Apply a comparison.
 * \param symbol The symbol indicates which comparison will be used.
 * \param lhs The left-hand side of the operation. It is an object.
 * \param rhd The right-hand side of the operation. It is an object.
 * \return An object, which is the result of the chosen comparison.
 * */
Object apply_comparison(
        const std::string& symbol,
        const Object& lhs,
        const Object& rhs);
