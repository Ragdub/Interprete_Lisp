/**\file object.hh
 * \brief File with definitions of Objects functions.
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
 
#pragma once

#include <iostream>
#include <string>

class Cell;
using Object = Cell*;

/** \return the boolean showing that the object \l is currently used or can 
 * be deleted.
 * \param l The object to test.*/
bool is_busy(const Object& l);

/** The object \l is considered usefull or not depending of the boolean 
 * \state.
 * \param l The object to modify.
 * \param state The boolean that caracterize the fact that the object is 
 * usefull.*/
void set_busy(const Object& l, bool state);

/**Creates an empty list
 * \return A pointer to the created cell.
 * */
Object empty();

/**Returns true if the object is the empty list
 * \param l The object to test.
 * */
bool null(const Object& l);

/**Creates a cell with the symbol "t"
 * \return The symbol "t"
 * */
Object t();


/**Append an object to the front of a list
 * \param a An object
 * \param l A list
 * \return The resulting list
 * */
Object cons(const Object& a, const Object& l);

/**Return the first object of a list
 * \param l A list
 * \return The first object of the list
 * */
Object car(const Object& l);

/**Return the queue of a list
 * \param l A list
 * \return The queue of the list
 * */
Object cdr(const Object& l);

/**Returns true if the two objects are equals.
 * \param a An object
 * \param b An object
 * \return True if a is equal to b.
 * */
bool eq(const Object& a, const Object& b);

/**Creates a object representing a number from an integer
 * \param n An integer.
 * */
Object number_to_Object(int n);

/**Creates a object representing a string from a C++ string
 * \param s A string.
 * \warning A C++ string can be used to create a string as well as a symbol
 * */
Object string_to_Object(const std::string& s);

/**Creates a object representing a symbol from a C++ string
 * \param s A string.
 * \warning A C++ string can be used to create a string as well as a symbol
 * */
Object symbol_to_Object(const std::string& s);

/**Creates an Object depending on the boolean: an empty list if false, 
 * the symbol "t" cell if true.
 * \param b A boolean.
 * */
Object bool_to_Object(bool b);

/**This function returns a number depending on the given object.
 * \param object The given object.
 * \return A number (an integer)
 * \warning The object must contain an integer.
 * */
int Object_to_number(const Object& l);
/**This function returns a number depending on the given object.
 * \param object The given object.
 * \return A number (an integer)
 * \warning The object must contain an integer.
 * */
std::string Object_to_string(const Object& l);

/**By using this function, you know if the chosen object is a number.
 * \param l The object to test.
 * \return The boolean is true if the object is a number.
 * */
bool is_number(const Object& l);
/**By using this function, you know if the chosen object is a string.
 * \param l The object to test.
 * \return The boolean is true if the object is a string.
 * */
bool is_string(const Object& l);
/**By using this function, you know if the chosen object is a symbol.
 * \param l The object to test.
 * \return The boolean is true if the object is a symbol.
 * */
bool is_symbol(const Object& l);
/**By using this function, you know if the chosen object is a list
 * (Cell_Pair)
 * \param l The object to test.
 * \return The boolean is true if the object is a pair.
 * */
bool is_list(const Object& l);
/**By using this function, you know if the chosen object is empty
 * (Cell_Empty)
 * \param l The object to test.
 * \return The boolean is true if the object is empty.
 * */
bool is_empty(const Object& l);

/**From a given object, this function applies to the ostream& s the 
 * correct transformation function depending on the type of the object's cell.
 * */
std::ostream& operator << (std::ostream& s, const Object& l);


Object cadr(const Object& l);
Object cddr(const Object& l);
Object caddr(const Object& l);
Object cdddr(const Object& l);
Object cadddr(const Object& l);
