#include "test.hh"

void test_assert(const std::string& description, bool test_pass) {
    std::string pass_fail_text;
    if (test_pass) {
        pass_fail_text = "Pass";
    } else {
        pass_fail_text = "Fail";
    }
    //We flush because this is for debuging
    std::cout << description << ": " << pass_fail_text << std::endl;
}

void header(const std::string& module_tested) {
    //We flush because this is for debuging
    std::cout << "######" << module_tested << "######" << std::endl;
}
