#include "test.hh"
#include "subroutine.hh"
#include "object.hh"

bool test_is_operator_true() {
    std::string operator_minus = "-";
    return is_operator(operator_minus);
}

bool test_is_operator_false() {
    std::string operator_what = "what";
    return !is_operator(operator_what);
}

bool test_apply_operator() {
    std::string operator_name = "+";
    Object lhs = number_to_Object(1);
    Object rhs = number_to_Object(2);
    return 3 == Object_to_number(apply_operator(operator_name, lhs, rhs));
}

bool test_is_comparison_true() {
    std::string comparator_inf_or_equal = "<=";
    return is_comparison(comparator_inf_or_equal);
}

bool test_is_comparison_false() {
    std::string comparator_what = "what";
    return !is_comparison(comparator_what);
}

bool test_apply_comparison() {
    std::string comparator_name = "<=";
    Object lhs = number_to_Object(1);
    Object rhs = number_to_Object(2);
    return apply_comparison(comparator_name, lhs, rhs);
}


void test_subroutine() {
    test_assert("Test if is_operator return true for operators",
            test_is_operator_true());
    test_assert("Test if is_operator return false for non operators",
            test_is_operator_false());
    test_assert("Test apply_operator", test_apply_operator());
    test_assert("Test if is_comparison return true for operators",
            test_is_comparison_true());
    test_assert("Test if is_comparison return false for non operators",
            test_is_comparison_false());
    test_assert("Test apply_comparison", test_apply_comparison());
}
