#pragma once

#include <iostream>
#include <string>

/**
 * \file test.hpp
 * \brief help for tests creation
 */


/**
 * Print the description of the test and if it passed or failed.
 * \param description description of the test that is asserted
 * \param test_pass boolean value that indicate if the test has failed
 */
void test_assert(const std::string& description, bool test_pass);

/**
 * Print a nice header for a section of test on the module denoted
 * by \a module_tested.
 * \param module_tested, name of the module being tested
 */
void header(const std::string& module_tested);
