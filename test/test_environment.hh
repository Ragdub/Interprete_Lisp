#include "test.hh"
#include "environment.hh"
#include "object.hh"

#include <memory>


bool test_link_get_value () {
	std::string test_symbol = "a";
	Link _link(test_symbol, number_to_Object(3));
	return eq(_link.get_value(), number_to_Object(3));
}

bool test_link_get_symbol () {
	std::string _symbol = "a";
	Link _link(_symbol, number_to_Object(3));
	return _link.get_symbol() == "a";
}

bool test_environment_extend () {
	std::string _symbol= "a";
	Link _link(_symbol, number_to_Object(3));

	std::shared_ptr<Environment_Base> _environment_empty = 
		std::make_shared<Environment_Empty>();

	std::shared_ptr<Environment_Base> _environment_ptr = 
		std::make_shared<Environment>(_link, _environment_empty	);

    std::shared_ptr<Environment_Base> _tested_environment =
        std::make_shared<Environment_Empty>();

	_tested_environment = _tested_environment->extend(_environment_ptr);

	return eq(_tested_environment->get_value(_symbol), number_to_Object(3));	
}

bool test_environment_get_value () {
	std::string _symbol = "a";
    std::shared_ptr<Environment_Base> _environment
        = std::make_shared<Environment_Empty>();

	_environment = _environment->add_link (_symbol, number_to_Object(2));
	_environment = _environment->add_link (_symbol, number_to_Object(3));

	return eq(_environment->get_value(_symbol), number_to_Object(3));
}


bool test_handle_extends_with_parameter_arguments (){
	std::string _symbol_a = "a";
	std::string _symbol_b = "b";
    std::shared_ptr<Environment_Base> _environment
        = std::make_shared<Environment_Empty>();
	Handle _handle;

	_environment = _environment->add_link (_symbol_a, number_to_Object(3));
	_environment = _environment->add_link (_symbol_b, number_to_Object(2));

	Object _arguments =
        cons(number_to_Object(3), cons(number_to_Object(2), empty()));

	Object _parameter =
        cons(symbol_to_Object("a"), cons(symbol_to_Object("b"), empty()));

	_handle.extends_with_parameter_arguments(_parameter, _arguments);

	return eq(_handle.get_value(_symbol_a), number_to_Object(3))
			&& 	eq(_handle.get_value(_symbol_b), number_to_Object(2));

}

void test_environment () {
    test_assert("Test of get_value in Link",
            test_link_get_value());
    test_assert("Test of get_symbol in Link",
            test_link_get_symbol());
    test_assert("Test of get_value in Environment",
            test_environment_get_value());
    test_assert("Test of extdend in Environment",
            test_environment_extend());
    test_assert("Test of extends_with_parameter_arguments in Handle",
            test_handle_extends_with_parameter_arguments());
}
