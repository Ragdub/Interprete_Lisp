verify_test() {
    echo -n "Testing $1: "
    if cat "./test/test_$1" | ./lisp | cmp --silent "./test/test_$1_result"; then
        echo "Pass";
    else
        echo "Fail";
    fi
}

verify_test "setq"
verify_test "if"
verify_test "apply"
verify_test "cond"
verify_test "defun"
verify_test "display"
verify_test "error"
verify_test "let"
verify_test "progn"
