/**\file environment.cc
 * \brief Methods of environment functions.
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */

#include <string>
#include <list>

#include "object.hh"
#include "cell.hh"
#include "environment.hh"
#include "util.hh"
#include "error.hh"


Link::Link (const std::string& _symbol, const Object& _value):
    symbol(_symbol), value(_value) {
}

std::string Link::get_symbol() const{
    return symbol;
}

Object Link::get_value() const{
    return value;
}

/*****************************/

std::shared_ptr<Environment_Base> Environment_Base::add_link(
        const std::string& _symbol,
        const Object& value) {
    set_busy(value, true);
    return std::make_shared<Environment>(
        Link(_symbol, value),
        shared_from_this());
}    

/*****************************/

bool Environment_Empty::is_empty() const {
    return true;
}

std::shared_ptr<Environment_Base> Environment_Empty::extend (
        const std::shared_ptr<Environment_Base>& environment) {
    return environment;
}

Object Environment_Empty::get_value (
        const std::string& _symbol) const {
    throw UnboundVariable(_symbol);
}

/*****************************/

Environment::Environment(
        const Link& link,
        const std::shared_ptr<Environment_Base>& environment):

    car(link), cdr(environment)
{}

bool Environment::is_empty() const {
    return false;
}

std::shared_ptr<Environment_Base> Environment::extend(
        const std::shared_ptr<Environment_Base>& environment) {

    if (environment->is_empty()) {
        return shared_from_this();
    }
    else {
        Link link = std::dynamic_pointer_cast<Environment>(environment)->car;
        std::shared_ptr<Environment_Base> enviroment_cdr =
            std::dynamic_pointer_cast<Environment>(environment)->cdr;

        //We extend with the queue, then we add the head which is missing
        return (extend(enviroment_cdr)->add_link(
            link.get_symbol(), link.get_value()));
    }
}

Object Environment::get_value (const std::string& _symbol) const {
    if (car.get_symbol() == _symbol) {
        return car.get_value();
    }
    else {
        return cdr->get_value(_symbol);
    }
}

Environment::~Environment() {
    set_busy(car.get_value(),false);
}

/*****************************/

void Handle::extends_with_parameter_arguments(
        const Object& parameter,
        const Object& arguments) {
    assert(get_list_length(parameter) == get_list_length(arguments));
    assert(is_empty(parameter) || is_symbol(car(parameter)));

    if (is_empty(parameter))
        return;

    Object first_parameter = car(parameter);
    std::string parameter_name = Object_to_string(first_parameter);
    Object first_argument = car(arguments);
    add_link(parameter_name, first_argument);
    extends_with_parameter_arguments(cdr(parameter), cdr(arguments));
}
    

void Handle::add_link(const std::string& _symbol, const Object& value){
    environment = environment->add_link(_symbol, value);
}

Object Handle::get_value(const std::string& _symbol) const {
    return environment->get_value(_symbol);
}

Handle::Handle():
    environment(std::make_shared<Environment_Empty>()) {
}

