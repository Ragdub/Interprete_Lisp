/**\file evaluation.cc
 * \brief Some really useful functions
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */

#include "util.hh"
#include <cassert>

std::size_t get_list_length(const Object& object) {
    assert(is_list(object) || is_empty(object));

    if (is_empty(object)) {
        return 0;
    }

    return 1 + get_list_length(cdr(object));
}

bool is_symbol_list(const Object& object) {
    if (is_empty(object)) {
        return true;
    }

    if (!is_list(object)) {
        return false;
    }

    if (!is_symbol(car(object))) {
        return false;
    }

    return is_symbol_list(cdr(object));
}

Object list_car (const Object& obj) {
    if (is_empty(obj)) {
        return obj;
    }
    else {
        return cons (car(car(obj)), list_car(cdr(obj)));
    }
}

Object list_cadr (const Object& obj) {
    if (is_empty(obj)) {
        return obj;
    }
    else {
        return cons (cadr(car(obj)), list_cadr(cdr(obj)));
    }
}
