/**\file subroutine.cc
 * \brief Methods of the subroutine functions.
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
#include "subroutine.hh"

/**\file subroutine.cc
 * \brief Methods of the subroutine functions.
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
#include "object.hh"
#include "error.hh"

#include <unordered_map>
#include <functional>

template<typename FunctionType>
using NameFunctionAssociation =
    std::unordered_map<std::string, std::function<FunctionType> >;

static NameFunctionAssociation<int(int, int)>
        operator_name_function_association = {
    { std::string("+"), std::plus<int>() },
    { std::string("*"), std::multiplies<int>() },
    { std::string("-"), std::minus<int>() },
    { std::string("/"), std::divides<int>() }
};

static NameFunctionAssociation<bool(int, int)>
        comparison_name_function_association = {
    { std::string("<"), std::less<int>() },
    { std::string(">"), std::greater<int>() },
    { std::string("="), std::equal_to<int>() },
    { std::string(">="), std::greater_equal<int>() },
    { std::string("<="), std::less_equal<int>() }
};

Object apply_subroutine(
        const std::string& symbol,
        const Object& lhs,
        const Object& rhs) {

    if (is_operator(symbol)) {
        return apply_operator(symbol, lhs, rhs);
    }
    else {
        return apply_comparison(symbol, lhs, rhs);
    }
}

bool is_subroutine(const std::string& symbol) {
    return is_operator(symbol) || is_comparison(symbol);
}

bool is_operator(const std::string& symbol) {
    return operator_name_function_association.find(symbol)
        != operator_name_function_association.end();
}

bool is_comparison(const std::string& symbol) {
    return comparison_name_function_association.find(symbol)
        != comparison_name_function_association.end();
}

Object apply_operator(
        const std::string& symbol,
        const Object& lhs,
        const Object& rhs) {
    NameFunctionAssociation<int(int, int)>::iterator operation_iterator =
        operator_name_function_association.find(symbol);
        
    if (!is_number(lhs) || !is_number(rhs)) {
        throw WrongArgumentError();
    }

    std::function<int(int, int)> operation = operation_iterator->second;
    return number_to_Object(operation( 
                Object_to_number(lhs),
                Object_to_number(rhs)));
}

Object apply_comparison(
        const std::string& symbol,
        const Object& lhs,
        const Object& rhs) {
    NameFunctionAssociation<bool(int, int)>::iterator comparison_iterator =
        comparison_name_function_association.find(symbol);
    
    if (!is_number(lhs) || !is_number(rhs)) {
        throw WrongArgumentError();
    }
    
    std::function<bool(int, int)> operation = comparison_iterator->second;
    return bool_to_Object(operation( 
                Object_to_number(lhs),
                Object_to_number(rhs)));
}

