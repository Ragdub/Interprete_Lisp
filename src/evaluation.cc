/**\file evaluation.cc
 * \brief Methods of evaluation functions.
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude, Initial Source
 * */

#include "evaluation.hh"
#include "error.hh"
#include "object.hh"
#include "subroutine.hh"
#include "environment.hh"
#include "util.hh"

Object map_eval(const Object& list, Handle& current_environment) {
    assert(is_list(list) || is_empty(list));
    if (is_empty(list)) {
        return list;
    }
    return cons(
            eval(car(list), current_environment),
            map_eval(cdr(list), current_environment));
}

bool check_number_arguments(const Object& object, std::size_t number_arguments) {
    return (get_list_length(object) >= number_arguments + 1);
}

Object eval_null(const Object& object, Handle& current_environment) {
    if (!check_number_arguments(object, 1)) {
        throw PrimitiveWrongNumberOfArgumentError("null", 1);
    }
    return bool_to_Object(is_empty(eval(cadr(object), current_environment)));
}

Object eval_cons(const Object& object, Handle& current_environment) {
    if (!check_number_arguments(object, 2)) {
        throw PrimitiveWrongNumberOfArgumentError("cons", 2);
    }

    Object second_arguments = eval(caddr(object), current_environment);
    if (!(is_list(second_arguments) || is_empty(second_arguments))) {
        throw PrimitiveWrongArgumentsError("cons", "anything, list");
    }
    return cons(
            eval(cadr(object), current_environment),
            second_arguments);
}

Object eval_car(const Object& object, Handle& current_environment) {
    if (!check_number_arguments(object, 1)) {
        throw PrimitiveWrongNumberOfArgumentError("car", 1);
    }

    Object argument = eval(cadr(object), current_environment);
    if (!is_list(argument)) {
        throw PrimitiveWrongArgumentsError("car", "list");
    }

    return car(argument);

}

Object eval_cdr(const Object& object, Handle& current_environment) {
    if (!check_number_arguments(object, 1)) {
        throw PrimitiveWrongNumberOfArgumentError("cdr", 1);
    }

    Object argument = eval(cadr(object), current_environment);
    if (!is_list(argument)) {
        throw PrimitiveWrongArgumentsError("cdr", "list");
    }

    return cdr(argument);
}



Object eval_subroutine(const Object& object, Handle& current_environment) {
    if (!check_number_arguments(object, 2)) {
        throw TooManyArgumentError();
    }
    std::string symbol_name = Object_to_string(car(object));
    Object lhs = eval(cadr(object), current_environment);
    Object rhs = eval(caddr(object), current_environment);
    return apply_subroutine(symbol_name, lhs, rhs);
}

Object eval_defun(const Object& object, Handle& current_environment) {
    Object modified_object = 
        cons(symbol_to_Object("setq"), 
            cons(cadr(object), 
                cons(cons(symbol_to_Object("lambda"), 
                    cddr(object)), empty())));

    return eval(modified_object, current_environment);
}

Object eval_setq(const Object& object, Handle& current_environment) {
    if (!check_number_arguments(object, 2)) {
        throw PrimitiveWrongNumberOfArgumentError("setq", 2);
    }
    Object link = cdr(object);
    std::string symbol = Object_to_string(car(link));
    Object value = cadr(link);
    current_environment.add_link(symbol, value);
    return empty();
}

bool check_if_argument(const Object& object) {
    return is_list(cdr(object)) && is_list(cdr(cdr(object)))
            && is_list(cdr(cdr(cdr(object))));
}

Object eval_if(const Object& object, Handle& current_environment) {
    if (!check_if_argument(object)){
        throw PrimitiveWrongNumberOfArgumentError("if", 3);
    }        
    Object condition = cadr(object);
    Object then_expression = caddr(object);
    Object else_expression = cadddr(object);
    if (is_empty(eval(condition, current_environment))) {
        return eval(else_expression, current_environment);
    }
    return eval(then_expression, current_environment);
}

Object eval_progn(const Object& object, Handle& current_environment) {
    Object expressions_to_execute = cdr(object);
    if (is_empty(expressions_to_execute)) {
        throw PrimitiveWrongNumberOfArgumentError("progn", 1);
    }
    while (!is_empty(cdr(expressions_to_execute))) {
        eval(car(expressions_to_execute), current_environment);
        expressions_to_execute = cdr(expressions_to_execute);
    }
    return eval(car(expressions_to_execute), current_environment);
}

Object eval_cond(const Object& object, Handle& current_environment) {
    Object expressions_to_execute = cdr(object);
    if (is_empty(expressions_to_execute)) {
        throw PrimitiveWrongNumberOfArgumentError("cond", 1);
    }
    while (!is_empty(expressions_to_execute)) {
        Object predicat = eval(car(car(expressions_to_execute)),
                current_environment);
        if (!is_empty(predicat)) {
            return eval(cadr(car(expressions_to_execute)),
                    current_environment);
        }
        expressions_to_execute = cdr(expressions_to_execute);
    }
    return empty();
}

Object eval_display(const Object& object, Handle& current_environment) {
    if (is_empty(cdr(object))) {
        throw PrimitiveWrongNumberOfArgumentError("display", 1);
    }
    std::cout << (eval(cadr(object), current_environment));
    std::cout << std::endl;
    return empty();
    }

Object eval_symbol(const Object& object, Handle& current_environment) {
    std::string symbol_name = Object_to_string(object);
    if (is_subroutine(symbol_name)) {
        return object;
    }
    if (symbol_name == "t") {
        return object;
    }
    return current_environment.get_value(symbol_name);
}

Object eval(const Object& object, Handle& current_environment) {
    //We suppose that we get an addition
    if (is_number(object) || is_string(object)) {
        return object;
    }
    if (is_empty(object)) {
        return object;
    }
    if (is_symbol(object)) {
        return eval_symbol(object, current_environment);
    }
    if (is_list(object)) {
        Object first_element = car(object);
        if (is_symbol(first_element)) {
            std::string symbol_name = Object_to_string(first_element);
            if (symbol_name == "defun") {
                return eval_defun(object, current_environment);
			}
            if (symbol_name == "setq") {
                return eval_setq(object, current_environment);
            }
            if (symbol_name == "quote") {
                return (cadr(object));
            }
            if (symbol_name == "let") {
                return eval(get_expansion_of_let(object), current_environment);
            }
            if (symbol_name == "display") {
                return eval_display(object, current_environment);
            }
            if (is_subroutine(symbol_name)) {
                return eval_subroutine(object, current_environment);
            }
            if (symbol_name == "if") {
                return eval_if(object, current_environment);
            }
            if (symbol_name == "progn") {
                return eval_progn(object, current_environment);
            }
            if (symbol_name == "cond") {
                return eval_cond(object, current_environment);
            }
            if (symbol_name == "car") {
                return eval_car(object, current_environment);
            }
            if (symbol_name == "cons") {
                return eval_cons(object, current_environment);
            }
            if (symbol_name == "cdr") {
                return eval_cdr(object, current_environment);
            }
            if (symbol_name == "null") {
                return eval_null(object, current_environment);
            }
        }
        //The first element must be a function
        Object arguments = map_eval(cdr(object), current_environment);
        return apply(first_element, arguments, current_environment);
    }
}

void check_is_function(const Object& object) {
    if (!is_list(object)) {
        throw ApplyNonListError();
    }
    Object first_element = car(object);
    if (!is_symbol(first_element)
            || Object_to_string(first_element) != "lambda") {

        throw LambdaMissingError();
    }
    if (!is_symbol_list(cadr(object))) {
        throw VariablesMissingError();
    }
}

Object get_expansion_of_let (const Object& object) {
    Object binding_list = cadr(object);
    Object body = cdr(cdr(object));
    Object parameters = list_car(binding_list);
    Object arguments = list_cadr(binding_list);
    return cons(
            cons(symbol_to_Object("lambda"),
                cons(parameters, body)),
            arguments);
}

Object apply(
        const Object& object,
        const Object& arguments,
        const Handle& current_environment) {

    if (is_symbol(object)) {
        std::string symbol_name = Object_to_string(object);
        Object function = current_environment.get_value(symbol_name);
        return apply(function, arguments, current_environment);
    }

    check_is_function(object);

    Object function_parameter = cadr(object);
    Object function_body = caddr(object);
    
    if (get_list_length(function_parameter) < get_list_length(arguments)) {
        throw TooManyArgumentError();
    }

    if (get_list_length(function_parameter) > get_list_length(arguments)) {
        throw MissingArgumentError();
    }
    
    Handle sub_environment(current_environment);
    sub_environment.extends_with_parameter_arguments(
            function_parameter,
            arguments);

    return eval(function_body, sub_environment);
}
