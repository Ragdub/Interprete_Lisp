/**\file object.cc
 * \brief Methods of the class Object.
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude, Initial source
 * */

#include <string>
#include <iostream>
#include <cassert>

#include "object.hh"
#include "cell.hh"
#include "garbage_collector.hh"


bool is_busy(const Object& l) {
    return l->is_busy();
}

void set_busy(const Object& l, bool state) {
    l->set_busy(state);
    if (is_list(l) && state) {
        set_busy(car(l), state);
        set_busy(cdr(l), state);
    }
}

Object empty() {
    static Cell *c = new Cell_Empty();
    return c;
}

bool null(const Object& l) {
    return l == empty();
}

Object t() {
    static Cell *c = new Cell_Symbol("t");
    return c;
}

Object cons(const Object& a, const Object& l) {
    Object pair = new Cell_Pair(a, l);
    GarbageCollector::get_singleton().add_object(pair);
    return pair;
}

Object car(const Object& l) {
    assert(is_list(l));
    return (dynamic_cast<Cell_Pair*>(l))->get_car();
}

Object cdr(const Object& l) {
    assert(is_list(l));
    return (dynamic_cast<Cell_Pair*>(l))->get_cdr();
}

Object number_to_Object(int n) {
    Object number = new Cell_Number(n);
    GarbageCollector::get_singleton().add_object(number);
    return number;
}

Object string_to_Object(const std::string& s) {
    Object string = new Cell_String(s);
    GarbageCollector::get_singleton().add_object(string);
    return string;
}


Object symbol_to_Object(const std::string& s) {
    Object symbol = new Cell_Symbol(s);
    GarbageCollector::get_singleton().add_object(symbol);
    return symbol;
}

Object bool_to_Object(bool b) {
    if (b) return t();
    return empty();
}

int Object_to_number(const Object& l) {
    assert(is_number(l));
    return (dynamic_cast<Cell_Number*>(l))->get_contents();
}

std::string Object_to_string(const Object& l) {
    assert(is_string(l) || is_symbol(l));
    if (is_string(l))
        return (dynamic_cast<Cell_String*>(l))->get_contents();
    if (is_symbol(l))
        return (dynamic_cast<Cell_Symbol*>(l))->get_contents();
    assert(false);
}

bool is_number(const Object& l) {
    assert(l != nullptr);
    return (l->get_sort() == Cell::sort::NUMBER);
}

bool is_string(const Object& l) {
    assert(l != nullptr);
    return (l->get_sort() == Cell::sort::STRING);
}

bool is_symbol(const Object& l) {
    assert(l != nullptr);
    return (l->get_sort() == Cell::sort::SYMBOL);
}

bool is_list(const Object& l) {
    assert(l != nullptr);
    return (l->get_sort() == Cell::sort::PAIR);
}

bool is_empty(const Object& l) {
    assert(l != nullptr);
    return l->get_sort() == Cell::sort::EMPTY;
}

static void print_aux(std::ostream& s, const Object& l);

std::ostream& operator << (std::ostream& s, const Object& l) {
    if (null(l)) return s << "()";
    if (is_number(l)) return s << Object_to_number(l);
    if (is_string(l)) return s << Object_to_string(l);
    if (is_symbol(l)) return s << Object_to_string(l);
    assert(is_list(l));
    s << "(";
    print_aux(s, l);
    s << ")";
    return s;
}

static void print_aux(std::ostream& s, const Object& l) {
    assert(is_list(l));
    if (null(l)) return;
    if (null(cdr(l))) {
        s << car(l);
        return;
    }
    s << car(l) << " ";
    print_aux(s, cdr(l));
}

Object cadr(const Object& l) {
    return car(cdr(l));
}
Object cddr(const Object& l) {
    return cdr(cdr(l));
}
Object caddr(const Object& l) {
    return car(cddr(l));
}
Object cdddr(const Object& l) {
    return cdr(cddr(l));
}
Object cadddr(const Object& l) {
    return car(cdddr(l));
}

bool eq(const Object& a, const Object& b) {
    assert(a != nullptr);
    assert(b != nullptr);
    if (a->get_sort() != b->get_sort()) return false;
    if (is_number(a)) return Object_to_number(a) == Object_to_number(b);
    if (is_string(a) || is_symbol(a)) return Object_to_string(a) == Object_to_string(b);
    return a == b;
}
