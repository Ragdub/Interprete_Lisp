/**\file read.cc
 * \brief Methods of reading functions.
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude, Initial source
 * */
 
#include <iostream>

#include "object.hh"
#include "read.hh"
#include "error.hh"

extern Object get_read_Object();
extern int yyparse();
extern void yyrestart(FILE *fh);

Object read_object() {
  if (yyparse() != 0)
      throw EndOfFileError();
  Object l = get_read_Object();
  std::cout << "Read: " << l << std::endl;
  return l;
}

void read_object_restart(FILE* fh) {
    yyrestart(fh);
}
