#include "error.hh"
#include <cstdlib>

EndOfFileError::EndOfFileError():
    std::runtime_error("End of file")
{}

LambdaMissingError::LambdaMissingError():
    std::runtime_error("Can't apply list without a lambda as first element")
{}

UnboundVariable::UnboundVariable(const std::string& symbol):
    std::runtime_error("Unbound variable " + symbol)
{}

VariablesMissingError::VariablesMissingError():
    std::runtime_error("Parameters of function must be a list of symbols")
{}

ApplyNonListError::ApplyNonListError():
    std::runtime_error("Can't apply non list object")
{}

TooManyArgumentError::TooManyArgumentError():
    std::runtime_error("To many arguments were given")
{}

MissingArgumentError::MissingArgumentError():
    std::runtime_error("Missing argument")
{}

WrongArgumentError::WrongArgumentError():
    std::runtime_error("Must be applied on a number")
{}

PrimitiveWrongNumberOfArgumentError::PrimitiveWrongNumberOfArgumentError(
        const std::string& function_name,
        int number_needed_arguments):
    std::runtime_error(
            function_name
            + " must have at least "
            + std::to_string(number_needed_arguments)
            + " arguments")
{}

PrimitiveWrongArgumentsError::PrimitiveWrongArgumentsError(
        const std::string& function_name,
        const std::string& needed_types):
    std::runtime_error(
            function_name
            + " has been given the wrong arguments."
            + " The arguments type: "
            + needed_types
            + ".")
{}



