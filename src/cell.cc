/**\file cell.cc
 * \brief Methods of the class Cell and its subtypes.
 * \version 1.2
 * \author Dylan Bellier, Jean Jouve, Axel Palaude, Initial source
 * */

#include <iostream>
#include "cell.hh"

static void trace(const std::string& s) {
  std::clog << s;
}

/*****************************/

bool Cell::is_busy() const {
    return (busy);
}

void Cell::set_busy(const bool& state) {
    busy = state;
}

static void trace(string s) {
  //clog << s;
}

/*****************************/

Cell_Number::Cell_Number(int _contents):
  contents(_contents){
  trace("n");
}

Cell::sort Cell_Number::get_sort() const {
  return sort::NUMBER;
}

int Cell_Number::get_contents() const {
  return contents;
}

/*****************************/

Cell_String::Cell_String(const std::string& _contents):
  contents(_contents){
  trace("t");
}

Cell::sort Cell_String::get_sort() const {
  return sort::STRING;
}

std::string Cell_String::get_contents() const {
  return contents;
}

/*****************************/

Cell_Symbol::Cell_Symbol(const std::string& _contents):
  contents(_contents){
  trace("s");
}

Cell::sort Cell_Symbol::get_sort() const {
  return sort::SYMBOL;
}

std::string Cell_Symbol::get_contents() const {
  return contents;
}

/*****************************/

Cell_Pair::Cell_Pair(Cell *_car, Cell *_cdr):
  car(_car), cdr(_cdr){
  trace("p");
}

Cell::sort Cell_Pair::get_sort() const {
  return sort::PAIR;
}

Cell *Cell_Pair::get_car() const {
  return car;
}

Cell *Cell_Pair::get_cdr() const {
  return cdr;
}

/*****************************/

Cell::sort Cell_Empty::get_sort() const {
    return sort::EMPTY;
}
