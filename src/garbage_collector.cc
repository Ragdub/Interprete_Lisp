/**\file garbage_collector.cc
 * \brief Methods of the subroutine functions.
 * \version 0.1
 * \author Dylan Bellier, Jean Jouve, Axel Palaude
 * */
#include "object.hh"
#include "environment.hh"
#include "garbage_collector.hh"

#include <vector>

GarbageCollector GarbageCollector::singleton;

void GarbageCollector::add_object(const Object& l) {
    object_collection.push_back(l);
}

void GarbageCollector::clean() {
    unsigned i = 0;
    while (i < object_collection.size()) {
        if (is_busy(object_collection.at(i))) {
            i++;
        }
        else {
            Object object_to_delete = object_collection.at(i);
            object_collection.at(i) = object_collection.back();
            object_collection.pop_back();
            delete object_to_delete;
        }
    }
}

GarbageCollector& GarbageCollector::get_singleton() {
    return GarbageCollector::singleton;
}
