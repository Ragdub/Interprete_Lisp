#include <iostream>
#include "object.hh"
#include "read.hh"
#include "error.hh"
#include "environment.hh"
#include "evaluation.hh"
#include "garbage_collector.hh"

int main() {
    Handle current_environment;
    while(true) {
        std::cout << "Lisp? " << std::flush;
        try {
            Object l = read_object();
            std::cout << eval(l, current_environment) << std::endl;
            GarbageCollector::get_singleton().clean();
        } 
        catch (const EndOfFileError& e) {
            std::cout << "End of File" << std::endl;
            return 0;
        } 
        catch (const std::runtime_error& e) {
            std::cout << e.what() << std::endl;
        }
    }
}
